package callcontrol;

import java.util.Arrays;

public final class PhoneNumber {

	private String countryCode = "";
	private String areaCode = "";
	private String subscriberNum = "";
	
	

	public PhoneNumber(String countryCode, String areaCode, String subscriberNum) {
		this.countryCode = validateCountryCode(countryCode);
		this.areaCode = removeLeedingZero(areaCode);
		this.subscriberNum = addSpace(subscriberNum);
	}

	static String validateCountryCode(String code) {

		code = code.replaceAll("([0,+])", "");
		
		return "+" + code;
		
		/*
		 * char[] countryCodeCharArr = code.toCharArray();
		 * 
		 * if (countryCodeCharArr[0] == '0') { if (countryCodeCharArr[1] == '0') {
		 * char[] newCountryCode = new char[this.countryCode.length() - 1];
		 * System.arraycopy(countryCodeCharArr, 2, newCountryCode, 1, 2);
		 * newCountryCode[0] = '+'; countryCode = Arrays.toString(newCountryCode); } }
		 

		return countryCode;*/
	}
	
	static String removeLeedingZero(String code) { 
		
		//return this.areaCode.substring(1, this.areaCode.lastIndexOf(this.areaCode));

		return code.replace("0", "");
	}
	
	static String addSpace(String code) {
		
		String tmp = code.substring(0,3);
		String tmpRest = code.substring(3);
		
		/*int length = subscriberNum.length();
		
		if (subscriberNum.charAt(3) != ' ') {
			char[] newSubscriberNum = new char[this.subscriberNum.length()+1];
			System.arraycopy(areaCode, 0, newSubscriberNum, 0, 3);
			System.arraycopy(areaCode, 3, newSubscriberNum, 4, length-3);
			subscriberNum = Arrays.toString(newSubscriberNum);
		}
		
		return this.subscriberNum;*/
		
		return tmp + " " + tmpRest;
		
	}
	
	public String toString() {
		
		StringBuilder str = new StringBuilder(this.countryCode + " " + this.areaCode + "/" + this.subscriberNum);
		String newPhoneNumString = str.toString();
		
		return newPhoneNumString;
	}
 	
	

}
