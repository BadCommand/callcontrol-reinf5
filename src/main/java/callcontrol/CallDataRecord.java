package callcontrol;

import java.time.LocalDateTime;

public class CallDataRecord {

	private PhoneNumber caller;
	private PhoneNumber called;
	private LocalDateTime start;
	private LocalDateTime end;
	private String str = "";

	public CallDataRecord(PhoneNumber caller, PhoneNumber called) {
		this.caller = caller;
		this.called = called;
	}

	public PhoneNumber getCalledParty() {

		return this.called;
	}

	public PhoneNumber getCallingParty() {

		return this.caller;
	}

	public LocalDateTime getStartTime() {

		return this.start;
	}

	public LocalDateTime getEndTime() {

		return this.end;
	}

	public void establish() {

		if (this.start == null)
			this.start = LocalDateTime.now();
	}

	public void disconnect() {

		if (this.end == null)
			this.end = LocalDateTime.now();
	}

	public String toString() {

		if (this.start == null && this.end == null) {
			str = "calling:" + " " + this.caller + ", " + "called:" + " " + this.called + ", " + "start: "
					+ "not yet established";
		} else if (this.start != null && this.end == null) {
			str = "calling:" + " " + this.caller + ", " + "called:" + " " + this.called + ", " + "start: "
					+ getStartTime() + ", " + "end:" + " still established";
		} else if (this.start != null && this.end != null) {
			str = "calling:" + " " + this.caller + ", " + "called:" + " " + this.called + ", " + "start: "
					+ getStartTime() + ", " + "end: " + getEndTime();
		}

		return str;
	}

}
