/*
 * Project and Training 1 - Computer Science, Berner Fachhochschule
 */

package callcontrol;

import java.time.LocalDateTime;

class CallControl {

	public PhoneNumber localParty;
	private CallDataRecord callRec;
	private CallState currState;

	public CallControl(PhoneNumber localParty) {
		this.localParty = localParty;
		this.currState = CallState.IDLE;
		;
	}

	public void release() {

		switch (currState) {
		case DISCONNECTED:
		case ALERTING:
		case DIALING:
		case IDLE:
			currState = CallState.RELEASED;
			break;
		default:
		}
	}

	public void dial(PhoneNumber otherParty) {

		if (this.currState == CallState.IDLE) {
			this.currState = CallState.DIALING;
			callRec = new CallDataRecord(this.localParty, otherParty);
		} 
	}

	public void alert(PhoneNumber otherParty) {
		
		if (this.currState == CallState.IDLE) {
			this.currState = CallState.ALERTING;
			callRec = new CallDataRecord(otherParty, this.localParty);
		} 
	}

	public void alert() {

		if (this.currState == CallState.DIALING) {
			currState = CallState.ALERTING;
		}
		
	}

	public void connect() {

		if (this.currState == CallState.ALERTING) {
			this.currState = CallState.CONNECTED;
			callRec.establish();
		}
	}

	public void disconnect() {

		if (this.currState == CallState.CONNECTED) {
			this.currState = CallState.DISCONNECTED;
			callRec.disconnect();
		}
	}

	public CallState getState() {

		return currState;
	}

	public CallDataRecord getCDR() {

		return callRec;
	}

	public enum CallState {

		ALERTING, CONNECTED, DIALING, DISCONNECTED, IDLE, RELEASED
	}

}
